#!/bin/bash

#  This script creates the binding site library.
#
#
# START WITH
#
#         run.sh ... create a new version of probisdb
#         run.sh update [data-old-dir]... update existing (if data-old-dir is given, then restart update with the specified data directory)
#

source src/defs.sh 
source src/func.sh 


# in case of an update, save old data directory
export OLD_DATA=""
if [[ $1 == "update" ]]
then
    if [[ $2 == "" ]]
    then
	OLD_DATA=$DATA-$(date|tr " " "-")
	mv -f $DATA $OLD_DATA
    else
	OLD_DATA=$2
    fi
fi

mkdir -p $TMP

echo "Starting Binding Site Library Creation (seq_id = $SEQID)"

#	echo "Update PDB"
#	01_update_pdb.sh
#	
#	echo "Prepare biological assemblies"
#	03_prepare_biounits.sh
#	
#	echo "Get clusters of 100% sequence id. from PDB"
#	05_getclust.sh $SEQID > $TMP/clusters$SEQID.txt
#	
echo "Get binding site chain id(s) of specific ligands"
20_neighb.sh

echo "Make protein surfaces"
30_make_protein_surfaces.sh $TMP/clusters$SEQID.txt

echo "Remove PDBDIR protein chains that produce defect SRF file"
40_remove_calpha.sh $TMP/clusters$SEQID.txt $TMP/clusters_pdb_$SEQID.txt $TMP/clusters_bio_$SEQID.txt

echo "Prepare bio files"
80_bio.sh $TMP/clusters_bio_$SEQID.txt

echo "Take only representative proteins to make bslib srf files"
90_bslib.sh $TMP/clusters_bio_$SEQID.txt

echo "Compare each WHOLE protein representative (90%) against bslibdb"
100_representatives.sh $TMP/clusters_pdb_$SEQID.txt

echo "If updating, join the new jsons with the old ones"
105_update.sh $TMP/clusters_pdb_$SEQID.txt

echo "Predict binding sites: grids, centroids, ligands... for all PDB proteins"
110_predict_bsites.sh

echo "Generate the genprobis database: mapping of snp to ligand interaction for all PDB proteins"
120_genprobis.sh $TMP/clusters_pdb_$SEQID.txt

echo "Generate docking ready binding sites across whole PDB"
130_dockready.sh


echo "Done."

