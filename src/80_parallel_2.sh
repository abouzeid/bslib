#!/bin/bash

{

    JSONFILE=$1
    name=$(basename $JSONFILE .json.gz)
    
    gsup --mols_name $name --biofile $TMP/$name.bio --bio none --models all --infile $JSONFILE --pdbdir $LIGDIR/pdb
    
    cat $TMP/$name.bio|sed -r "s/(^HETATM.{72}).*/\1/"|sed -r "s/(^ATOM.{74}).*/\1/" > $LIGDIR/bio/$name.pdb
    gzip -q $LIGDIR/bio/$name.pdb
    
    rm -f $TMP/$name.bio


} &>> $TMP/dbg80.txt


