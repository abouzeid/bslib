#!/bin/bash

export CLUSTER_FILE=$1
export GTMP=$TMP/genprobis

rm -Rf $GTMP
mkdir -p $GTMP
 
# retrieve database files
echo "Retrieving SIFTS with command rsync -q -rlpt -z --delete $FTP_SIFTS $SIFTS"
rsync -q -rlpt -z --delete $FTP_SIFTS $SIFTS

echo "Retrieving UniProt Variants ..."
mkdir -p $UNIPROT/variants
retry "wget -q -r -N -nd -P $UNIPROT/variants $FTP_UNIPROT_VARIANTS"
retry "wget -q -r -N -nd -P $UNIPROT/variants ftp://ftp.expasy.org/databases/uniprot/current_release/knowledgebase/variants/"

echo "Retrieving ClinVar ..."
mkdir -p $CLINVAR
wget -q -O $CLINVAR/clinvar.xml.gz $FTP_CLINVAR

echo "Retrieving PharmGKB (requires unzip)..."
mkdir -p $PHARMGKB
wget -q -O $PHARMGKB/pharmgkb.zip $HTTPS_PHARMGKB
unzip -o $PHARMGKB/pharmgkb.zip -d $PHARMGKB

echo "Retrieving more UniProt info..."
mkdir -p $UNIPROT/extra
wget -O $UNIPROT/extra/uniprot.tab "http://www.uniprot.org/uniprot/?query=database:(type:pdb)&columns=id,genes,organism&format=tab"

# generate a list of all pdb and chain ids
cut -f3 $CLUSTER_FILE|sed -r "s/(.{4}):(.)/\L\1\E,\2/" > $GTMP/pdbchains.txt
#    
# run genprobis to create mapping between uniprot, pdb, snp, and interactions
echo "Running GenProBiS ..."
genexdb --tmp_dir $GTMP --uniprot_variants_dir $UNIPROT/variants --sifts_dir $SIFTS --clinvar_file $CLINVAR/clinvar.xml.gz --pharmgkb_file $PHARMGKB/rsid.tsv --uniprot_file $UNIPROT/extra/uniprot.tab --clinvar_json_file $GTMP/clinvar.json --uniprot_json_file $GTMP/uniprot.json --pharmgkb_json_file $GTMP/pharmgkb.json > $TMP/dbg120.txt

#
all_worknodes "rsync -aRO --delete $BASE/bin $BASE/src SUBST:/" "Distribute base file to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $GTMP SUBST:/" "Distribute base file to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $SIFTS SUBST:/" "Distribute base file to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $PDBDIR SUBST:/" "Distribute base file to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $INTE $EVO $LIGS SUBST:/" "Distribute base file to nodes" $WORKNODE_LIST

cat $GTMP/pdbchains.txt|shuf|parallel --progress --delay 0.1 --load 100% --colsep ',' --env PATH --env TMP --env GENPROBISDB --env GTMP --env SIFTS --env PDBDIR --env INTE --env EVO --env LIGS --workdir $GTMP/work --sshloginfile $WORKNODE_LIST --return $GENPROBISDB/json/{1}{2}.json.gz --return $GENPROBISDB/tsv/{1}{2}.tsv.gz "timeout 10h 120_parallel.sh {1} {2}"


# generate the whole database
echo "Generating the whole database"
mkdir -p $GENPROBISDB/whole

for i in $(ls $GENPROBISDB/tsv|head -1)
do
    zegrep "^#|^evolution" $GENPROBISDB/tsv/$i|gzip
done > $GENPROBISDB/whole/genprobisdb.tsv.gz

for i in $(ls $GENPROBISDB/tsv)
do
    zegrep -v "^#|^evolution" $GENPROBISDB/tsv/$i|gzip
done >> $GENPROBISDB/whole/genprobisdb.tsv.gz

