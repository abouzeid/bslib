#!/bin/bash

export CLUSTER_FILE=$1

mkdir -p $REPDIR
rm -f $TMP/dbg90.txt
for pdbchain in $(sort -nusk1,1 $CLUSTER_FILE|tr -d " "|tr "\t" "_"|cut -f3 -d"_"|sed -r "s/(.{4}):(.{1})$/\L\1\E\2/")
do
    rep=$pdbchain.srf
    gunzip -c $SRFDIR/$rep.gz > $REPDIR/$rep 2>> $TMP/dbg90.txt
    [ ! -s $REPDIR/$rep ] && rm -f $REPDIR/$rep
done

