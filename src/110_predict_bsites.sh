#!/bin/bash


ls $JSON2|grep ".json.gz$" > $TMP/modified_jsons.txt

if [[ $OLD_DATA != "" ]]
then
    for i in $(ls $JSON2|grep ".json.gz$")
    do
	# if the old JSON file is missing, or if the JSONs differ, or if clusters have changed...
	python 110_check.py $JSON2/$i $OLD_DATA/probisdb/json/$i $LIGDIR/bio $OLD_DATA/lig/bio &> /dev/null

	if [ $? -ne 0 ]
	then
	    echo $i
	fi

    done > $TMP/modified_jsons.txt

fi

# distribute files
all_worknodes "rsync -aRO --delete --rsync-path=\"mkdir -p $BASE && rsync\" --exclude=$(basename $DATA) --exclude=\"data-*\" $BASE SUBST:/" "Distribute base files" $WORKNODE_LIST

all_worknodes "ssh SUBST \"rm -f $TMP/dbg110.txt\"" "Removing old debug files" $WORKNODE_LIST
all_worknodes "ssh SUBST \"rm -Rf $EVO $INTE $CENTRO $GRID $LIGS $SUPER $SURF $BRESI\"" "Removing any previous probisdb files" $WORKNODE_LIST

all_worknodes "rsync -aRO --delete --rsync-path=\"mkdir -p $TMP $EVO $INTE $CENTRO $GRID $LIGS $SUPER $SURF $BRESI && rsync\" $JSON2 SUBST:/" "Distribute json files" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $PDBDIR $SRFDIR $LIGDIR SUBST:/" "Distribute pdb and srf files to nodes" $WORKNODE_LIST


cat $TMP/modified_jsons.txt|parallel --env LIGDIR --env GRIDSPACING --env EVO --env INTE --env BRESI --env SURF --env GRID --env CENTRO --env LIGS --env SUPER --env REPDIR --env ION_NUMOCC --env WATER_NUMOCC --env PATH --env PDBDIR --env TMP --env JSON2 --progress --delay 1.0 --jobs 100% --sshloginfile $WORKNODE_LIST 'timeout 10h 110_parallel.sh {}'

# collect files
all_worknodes "rsync -aRO SUBST:$PROBISDB /" "Collecting predicted binding sites and ligands" $WORKNODE_LIST

