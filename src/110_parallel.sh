#!/bin/bash

{
    pdbchain=$(basename $1 .json.gz)
    pdb_id=${pdbchain:0:4}
    chain_id=${pdbchain:4:1}

    echo "Calculating protein ${pdb_id}${chain_id}..."

    probislite --noprobis --inte_dist 5.0 --bio $LIGDIR/bio --grid $GRIDSPACING --json $JSON2/$pdbchain.json.gz --evo_file $EVO/evo_$pdbchain.json --inte_file $INTE/inte_$pdbchain.json --grid_file $GRID/grid_$pdbchain.json --centro_out $CENTRO/centro_$pdbchain.json --lig_clus_file $LIGS/ligands_$pdbchain.json --superimpose_file $SUPER/super_$pdbchain.json --surf_file $SURF/surf_$pdbchain.json --bresi_file $BRESI/bresi_$pdbchain.json --srf_dir $REPDIR --ion_num_occ $ION_NUMOCC --water_num_occ $WATER_NUMOCC --receptor $PDBDIR/all/pdb/pdb$pdb_id.ent.gz --receptor_chain_id $chain_id

    gzip -f $EVO/evo_$pdbchain.json $INTE/inte_$pdbchain.json $GRID/grid_$pdbchain.json $CENTRO/centro_$pdbchain.json $LIGS/ligands_$pdbchain.json $SUPER/super_$pdbchain.json $SURF/surf_$pdbchain.json $BRESI/bresi_$pdbchain.json

} &>> $TMP/dbg110.txt
