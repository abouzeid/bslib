#!/bin/bash


function retry {
    cmd=$1
    while [[ $($cmd &>/dev/null;echo $?) != "0" ]]
    do
	(>&2 echo "Retrying to execute $cmd...")
	sleep 10
    done
    (>&2 echo "Successfully executed $cmd")
}

function all_worknodes {
    cmd=$1
    prt=$2
    worknodes=$3
    servers=""
    while read line
    do
	servers="$servers $(echo "$line" | cut -d "/" -f 2)"
    done < $worknodes
    
    for server in $servers
    do
	realcmd=$(echo "$cmd"|sed "s/SUBST/$server/g")
	echo $server ":" $prt " executing cmd =" $realcmd
	eval $realcmd
    done
}

confirm () {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
	[yY][eE][sS]|[yY])
	    true
	    ;;
	*)
	    false
	    ;;
    esac
}


export -f retry
export -f all_worknodes
export -f confirm
