#!/bin/bash

#
# Dobimo clusters95.txt, ki je ze urejen tako, da so predstavniki na prvem mestu
#
#
#
#

SEQUENCEID=$1

# dobimo predstavnike
retry "wget -q -O $TMP/c$SEQUENCEID.tmp https://www.rcsb.org/pdb/rest/representatives?cluster=$SEQUENCEID"

cat $TMP/c$SEQUENCEID.tmp|egrep -v "\".{4}\.[^\"]{2}\""|sed 's/\(.*\)\(.\{4\}\..\)\(.*\)/\2/'|grep -v representatives > $TMP/c2$SEQUENCEID.tmp
#dobimo klastre
retry "wget -q -O $TMP/bc-$SEQUENCEID.out ftp://resources.rcsb.org/sequence/clusters/bc-$SEQUENCEID.out"
# naredimo v format "starega" clusters95.txt
cnt=0
for i in $(cat $TMP/c2$SEQUENCEID.tmp)
do
    query=$(echo $i|tr "." "_");
    echo $query|tr "_" ":"|nl -v1 -nln|sed "s/^/$cnt\t/"
    grep $query $TMP/bc-$SEQUENCEID.out|tr "[:space:]" "\n"|egrep "^.{4}_.$"|grep -v $query|tr "_" ":"|sed "/^$/d"|nl -v2 -nln|sed "s/^/$cnt\t/"
    cnt=$((cnt+1))
done
rm -f $TMP/c$SEQUENCEID.tmp $TMP/c2$SEQUENCEID.tmp $TMP/bc-$SEQUENCEID.out
