#!/bin/bash

{
    pdb_id=$1
    chain_id=$2

    echo "Calculating UniProt to PDB mapping for protein ${pdb_id}${chain_id}..."

    genmapping --tmp_dir $GTMP --clinvar_json_file $GTMP/clinvar.json --uniprot_json_file $GTMP/uniprot.json --pharmgkb_json_file $GTMP/pharmgkb.json --inte_dir $INTE --evo_dir $EVO --ligands_dir $LIGS --pdb_dir $PDBDIR/all/pdb --sifts_dir $SIFTS --pdb_id $pdb_id --chain_id $chain_id --out_json_dir $GENPROBISDB/json --out_tsv_dir $GENPROBISDB/tsv

    gzip -f $GENPROBISDB/json/$pdb_id$chain_id.json $GENPROBISDB/tsv/$pdb_id$chain_id.tsv


} &>> $TMP/dbg120.txt
