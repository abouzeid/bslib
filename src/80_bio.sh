#!/bin/bash

#
# Naredimo .bu.pdb in .lig datoteke!
#
#
#
#


export CLUSTER_FILE=$1

# Obnovimo bazo ligandov - superponiramo vse v vsakem spremenjenem klastru na njihovega predstojnika :)

mkdir -p $LIGDIR/bio $LIGDIR/nosql $LIGDIR/json
rm -f $TMP/dbg80.txt

# go over all representatives in cluster file in a parallel job
sort -nusk1,1 $CLUSTER_FILE|tr -d " "|tr "\t" "_"|parallel --progress "timeout 2h 80_parallel_1.sh"

# deal with alignments...json in 10K bins - naredimo biounite in jih prilegamo na biounit pedstavnika (pustimo samo ligande in predstavnikov chain
find $LIGDIR/json -type f|grep "\.json.gz$"|shuf|parallel --progress "timeout 1h 80_parallel_2.sh"
