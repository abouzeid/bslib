#!/bin/bash

{

    PDB=$1
    PDBID=${PDB:3:4}
    ID=${PDB:4:2}

    # generate the first bio assembly from zipped pdb
    gbio --qpdb $PDBDIR/all/pdb/$PDB --bio first --models first --biofile $TMP/${PDBID}_bio.pdb

    zegrep -v "^ATOM|^HETATM|^END|^TER|^CONECT|^MASTER|^MODEL" $PDBDIR/all/pdb/$PDB > $TMP/${PDBID}_head.pdb
    
    # take the last BIO ASSEMBLY (since we generated only one bioassembly, this is actually the first one) or ASYM unit if no BIO is available
    tac $TMP/${PDBID}_bio.pdb|grep -B10000000 -m1 "^#ASSEMBLY"|tac|egrep "^ATOM|^HETATM|^CONECT|^TER|^MODEL|^ENDMDL" > $TMP/${PDBID}_body.pdb
    
    cat $TMP/${PDBID}_head.pdb $TMP/${PDBID}_body.pdb > $TMP/${PDBID}_whole.pdb
    
    # compute molecule types, i.e., glycan, cofactor etc. for pdb file
    moltype -i $TMP/${PDBID}_whole.pdb -o $TMP/${PDBID}_typed.pdb

    cat $TMP/${PDBID}_head.pdb $TMP/${PDBID}_typed.pdb |gzip -1 > $BIODIR/${PDBID}.pdb.gz

    rm -f $TMP/${PDBID}_*.pdb

} &>> $TMP/dbg03.txt
