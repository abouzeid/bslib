#!/bin/bash

export LIGTYPE="all" # compound, protein, nucleic, ion, water, all

export SEQID=100;
export MIN_ATOM_NUM=7;
export DIST=5.0;
export BASE=$(pwd);
export DATA="$BASE/data";
export TMP="$DATA/tmp";

export SRFDIR="$DATA/srf";
export REPDIR="$DATA/repsrf";
export LIGDIR="$DATA/lig";
export BIODIR="$DATA/biounits";
export NOSQL="$DATA/nosql";
export PDBDIR="$DATA/pdb";

export SIFTS="$DATA/sifts";
export UNIPROT="$DATA/uniprot";
export CLINVAR="$DATA/clinvar";
export PHARMGKB="$DATA/pharmgkb";

export PROBISDB="$DATA/probisdb";
export NOSQL2="$PROBISDB/nosql";
export JSON2="$PROBISDB/json";
export CENTRO="$PROBISDB/centro";
export GRID="$PROBISDB/grid";
export LIGS="$PROBISDB/ligands";
export SUPER="$PROBISDB/super";
export SURF="$PROBISDB/surf";
export BRESI="$PROBISDB/bresi";
export INTE="$PROBISDB/inte";
export EVO="$PROBISDB/evo";

export DOCKREADY="$DATA/dockready";

export GENPROBISDB="$DATA/genprobisdb";

export GRIDSPACING=1.5;
export CLUSRAD=2.0;
export ION_NUMOCC=10;
export WATER_NUMOCC=10;

export WORKNODE_LIST="worknodes.txt";

export PATH="$PATH:$(pwd):$(pwd)/src:$(pwd)/bin";

export FTP_PDB_ALL="ftp.wwpdb.org::ftp_data/structures/all/pdb/";
export FTP_PDB_DIV="ftp.wwpdb.org::ftp_data/structures/divided/pdb/";
export FTP_SIFTS="ftp.ebi.ac.uk::pub/databases/msd/sifts/split_xml/";
export FTP_UNIPROT_VARIANTS="ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/";
export FTP_CLINVAR="ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/xml/ClinVarFullRelease_00-latest.xml.gz";
export HTTPS_PHARMGKB="https://api.pharmgkb.org/v1/download/file/data/rsid.zip";

export NUMPROBIS=$(grep ^processor /proc/cpuinfo|wc -l);
export LC_ALL="en_US.utf-8";


