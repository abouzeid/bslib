#!/bin/bash

rm -Rf $LIGDIR/pdb $TMP/dbg20.txt
mkdir -p $LIGDIR/pdb

# distribute files over worknodes
all_worknodes "rsync -aRO --delete $BASE SUBST:/ --exclude=\"data-*\" " "Distribute all files to nodes" $WORKNODE_LIST

ls $BIODIR|parallel --progress --env PATH --env TMP --env BIODIR --env LIGDIR --env BASE --env DIST --sshloginfile $WORKNODE_LIST "timeout 2h 20_parallel.sh {}"

# collect the data from all hosts
all_worknodes "rsync -aRO SUBST:$LIGDIR/pdb /" "Collecting ligand pdb files from nodes" $WORKNODE_LIST

# gzip stray unzipped pdbs
gzip -q $LIGDIR/pdb/*.pdb
