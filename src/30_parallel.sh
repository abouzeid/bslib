#!/bin/bash

{
    line=$1

    clus_num=$(echo "$line"|cut -f1)
    num_in_clus=$(echo "$line"|cut -f2)
    pdb_id=$(echo "$line"|cut -f3|cut -f1 -d":"|tr '[:upper:]' '[:lower:]')
    chain_id=$(echo "$line"|cut -f3|cut -f2 -d":")

    pdb_file=$pdb_id$chain_id.pdb
    srf_file=$pdb_id$chain_id.srf

    # only do comp. intensive probis surface extraction if there is NO srf or srf is empty 
    [ -s $SRFDIR/$srf_file.gz ] && exit

    # get pdb file from bio database
    gunzip -c $BIODIR/$pdb_id.pdb.gz|awk 'BEGIN{b="^ATOM|^HETATM"} $1 ~ "ENDMDL" {b="^ATOM"} $1 ~ b { print $0 } $1 ~ "^TER|^MODEL|^ENDMDL" { print $0 }' > $TMP/$pdb_file

    echo "cmd = probis -extract -f1 $TMP/$pdb_file -c1 $chain_id -srffile $SRFDIR/$srf_file"

    # probis creates surface ONLY from the first model..
    probis -extract -f1 $TMP/$pdb_file -c1 $chain_id -srffile $SRFDIR/$srf_file
    
    # gzip the surface file
    gzip $SRFDIR/$srf_file

    rm -f $TMP/$pdb_file
    
} &>> $TMP/dbg30.txt
