import json
import sys
import gzip

## argument 1 ... old json.gz file from previous update
## argument 2 ... new json.gz file from this update
## argument 3 ... filter file with pdb&chain id of new bslib on each line

#read the old and new json files
with gzip.open(sys.argv[1], "rb") as f:
    data1 = json.loads(f.read().decode("ascii"))
    
with gzip.open(sys.argv[2], "rb") as f:
    data2 = json.loads(f.read().decode("ascii"))

new_bslib = {}

#read the filter file with new bslib pdbs
with open(sys.argv[3]) as filter_file:
    for pdbchain in filter_file:
        pdbchain = pdbchain.replace("\n", "")
        new_bslib[pdbchain] = 1
        
#leave just those pdbs that are in the new bslib (do not allow duplicates)
result=[]
for d in data1:
    if d["pdb_id"] in new_bslib:
        result.append(d)
        new_bslib.pop(d["pdb_id"])

for d in data2:
    if d["pdb_id"] in new_bslib:
        result.append(d)
        new_bslib.pop(d["pdb_id"])

print json.dumps(result, separators=(',', ':'))
