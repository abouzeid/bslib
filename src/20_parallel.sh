#!/bin/bash

    
{

    pdb_file=$1
    pdb_id=${pdb_file:0:4}

    rm -f $LIGDIR/pdb/$pdb_id*

    lig_dir=$TMP/$pdb_id
    rec_dir=$TMP/receptor_${pdb_id}

    mkdir -p  $lig_dir $rec_dir

    file_contents=$(gunzip -c $BIODIR/$pdb_file)

    echo "$file_contents"|egrep "compound|cofactor|glycan|ion|water|protein|nucleic|^END|^CONECT"|awk -v pdb_id=$pdb_id -v tmp_dir=$lig_dir -f $BASE/src/split.awk


    # get receptor for water, ion and compound by removing the ligand from the 1st model
    echo "$file_contents"|zegrep "protein|^END"|grep -m 1 -B 999999 "^END" >  $rec_dir/receptor.pdb

    
    for file in $(ls $lig_dir)
    do

	ltype=$(echo $file|cut -f1 -d"_")
	# pdb_id already initialized
	resn=$(echo $file|cut -f3 -d"_")
	resi=$(echo $file|cut -f4 -d"_")
	chain_id=$(echo $file|cut -f5 -d"_")
	model_num=$(echo $file|cut -f6 -d"_")
	
	pdbname=${ltype}_${pdb_id}_${resn}_${resi}_${chain_id}_${model_num}
	
	receptor_file=$rec_dir/receptor.pdb

	# for PROTEIN-ligands that are from the FIRST MODEL set a DIFFERENT receptor, in which the protein-ligand is removed from the 1st model
	# for protein-ligands that are from MODEL > 1 take the default receptor (entire first model protein)
	if [[ $ltype == "protein" ]] && [[ $model_num == 1 ]]
	then
		receptor_file=$rec_dir/receptor_$pdbname.pdb
		echo "$file_contents"|awk -v chain_id=$chain_id 'BEGIN{i=1;} $1 ~ "^ENDMDL" {i++;} i == 1 && match($0,"protein") && substr($0,22,1) != chain_id && $1 ~ "^ATOM" { print $0 } ' > $receptor_file
	    
	fi 
	
	# take only the chain that has most contacts with the ligand
	all_bs_chain_ids=$(neighb $lig_dir/$file $receptor_file $DIST|cut -f2 -d":")
	
	freq_chain_id=$(echo "$all_bs_chain_ids"|sort|uniq -c|sort -k1nr|sed -r "s/^\s+(.*)/\1/"|tr " " "_")

	for rec in $freq_chain_id
	do
	    
	    num_contact_atoms=$(echo "$rec"|cut -f1 -d"_")
	    bs_chain_id=$(echo "$rec"|cut -f2 -d"_")

	    ligc="${ltype}_${pdb_id}_${bs_chain_id}_${resn}_${resi}_${chain_id}_${model_num}"

	    echo $ligc $num_contact_atoms $bs_chain_id

	    # if number of contact atoms is greater than N, with exception of glycans, which have no restriction
	    if [[ $ltype == "glycan" ]] || ([[ $ltype == "cofactor" ]] && (( $num_contact_atoms > 20 ))) || ([[ $ltype == "water" ]] && (( $num_contact_atoms > 3 ))) || ([[ $ltype == "protein" ]] && (( $num_contact_atoms > 100 ))) || ([[ $ltype == "nucleic" ]] && (( $num_contact_atoms > 50 ))) || ([[ $ltype == "compound" ]] && (( $num_contact_atoms > 20 ))) || ([[ $ltype == "ion" ]] && (( $num_contact_atoms > 3 )))
	    then

		header="#MOLECULE $ligc\n#ASSEMBLY ASYMMETRIC UNIT 0\nMODEL        1\n"
		body=$(cat $lig_dir/$file)"\n"
		footer="TER\nENDMDL\nEND"
		
		echo -e "$header$body$footer" >> $LIGDIR/pdb/$pdb_id$bs_chain_id.pdb
		
	    else
		echo "Discarding $ligc because it has only $num_contact_atoms atoms in contact with receptor"
	    fi
	done
	
    done
    
    gzip -q $LIGDIR/pdb/$pdb_id*.pdb
    
    rm -Rf $lig_dir $rec_dir
    
} &>> $TMP/dbg20.txt
