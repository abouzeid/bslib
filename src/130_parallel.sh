#!/bin/bash
    
{

    pdb_file=$1
    pdb_id=${pdb_file:0:4}

    results=$DOCKREADY/${pdb_id}
    mkdir -p $results

    dockready --receptor $BIODIR/$pdb_file --pdb_id $pdb_id --lig_clus_dir $LIGS --legend_file $results/legend_${pdb_id}.txt --vina_centro_file $results/vina_centro_${pdb_id}.txt  --probisdock_centro_file $results/probisdock_centro_${pdb_id}.txt  --probisdock_ligands_file $results/probisdock_ligands_${pdb_id}.pdb --dockready_receptor_file $results/receptor_${pdb_id}.pdb
    
} &>> $TMP/dbg130.txt
