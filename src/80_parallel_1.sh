#!/bin/bash

{
    cluster_number=$(echo "$1"|cut -f1 -d"_")
    rep=$(echo "$1"|cut -f3 -d"_"|sed -r "s/(.{4}):(.{1})$/\L\1\E\2/")
    
    workdir=$TMP/${cluster_number}_srfs_80
    mkdir -p $workdir
    
    egrep "^$cluster_number\s+" $CLUSTER_FILE|cut -f3|sed "s,^,$workdir/,"|sed -r "s/(.{4}):(.{1})$/\L\1\E\2.srf \2/" > $TMP/$rep.srfs.80    
    
    #prepare surface database in place
    for i in $(cat $TMP/$rep.srfs.80|tr " " "@")
    do
	srf_file=$(echo $i|cut -f1 -d"@")
	srf_file_no_path=$(basename $srf_file)
	pdb_id=$(echo $(basename $srf_file)|cut -f1 -d".")
	gunzip -c $SRFDIR/$srf_file_no_path.gz > $workdir/$srf_file_no_path
    done
    
    rep_srf_file=$(grep $rep $TMP/$rep.srfs.80|cut -f1 -d" ")
    rep_chain_id=$(grep $rep $TMP/$rep.srfs.80|cut -f2 -d" ")
    
    # prilegamo vse na predstojnika -> nosql
    nosql=$(basename $rep_srf_file .srf).nosql
    
    rm -f $LIGDIR/nosql/$nosql
    probis -ncpu 1 -surfdb -local -sfile $TMP/$rep.srfs.80 -longnames -out $LIGDIR/nosql -nosql $nosql -f1 $rep_srf_file -c1 $rep_chain_id

    # naredimo json file
    json=$(basename $nosql .nosql).json
    pdb_id=${nosql:0:4}
    chain_id=${nosql:4:1}

    gunzip -c $BIODIR/$pdb_id.pdb.gz > $TMP/$pdb_id.pdb
    
    probis -nofp -longnames -results -param parameters.inp -nosql $LIGDIR/nosql/$nosql -out $LIGDIR/json -json $json -f1 $TMP/$pdb_id.pdb -c1 $chain_id

    gzip $LIGDIR/nosql/$nosql $LIGDIR/json/$json

    #cleanup
    rm -Rf $TMP/$pdb_id.pdb $LIGDIR/json/*.pdb
    rm -Rf $workdir $TMP/$rep.srfs.80

} &>> $TMP/dbg80.txt

