#!/bin/bash

#
# Odstranimo iz clusters95.txt tiste proteine, ki so samo iz CA atomov, ali C CA N CB modeli, ali ni srf-ja
# Nujno za srf.sh! V SRFDIR morajo biti srf-ji za celoten PDB, ne samo za nr-PDB !!!!
#
#
#

export CLUS_FILE=$1
export CLUS_FILE_MOD_PDB=$2
export CLUS_FILE_MOD_BIO=$3

rm -f $CLUS_FILE_MOD_PDB $CLUS_FILE_MOD_BIO $TMP/dbg40.txt

# Iz clusters file-a izbrisemo proteine, za katere SRF-ja ni, ali je defekten
cat $CLUS_FILE|tr '\t' '_'|parallel --progress 40_parallel.sh

# sortiramo oba mod clusters filea
sort -k1 -k2 -n $CLUS_FILE_MOD_PDB -o $CLUS_FILE_MOD_PDB
sort -k1 -k2 -n $CLUS_FILE_MOD_BIO -o $CLUS_FILE_MOD_BIO

