#!/bin/bash

mkdir -p $PDBDIR/all/pdb 
mkdir -p $PDBDIR/divided/pdb 

rsync -q -rlpt -z --delete --port=33444 $FTP_PDB_ALL $PDBDIR/all/pdb &> $TMP/dbg01.txt
rsync -q -rlpt -z --delete --port=33444 $FTP_PDB_DIV $PDBDIR/divided/pdb &>> $TMP/dbg01.txt
