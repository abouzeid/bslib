#!/bin/bash

{
    gzjson=$1.json.gz
    
    if [[ $(ls $OLD_DATA/probisdb/json/$gzjson 2>/dev/null) != "" ]] && [[ $(ls $JSON2/$gzjson 2>/dev/null) != "" ]]
    then
	echo "oba $gzjson"
	python $BASE/src/105_join_jsons.py $OLD_DATA/probisdb/json/$gzjson $JSON2/$gzjson $TMP/pdbs_from_bslib.txt|gzip > $TMP/json_new/$gzjson
    elif [[ $(ls $JSON2/$gzjson 2>/dev/null) != "" ]]
    then
	echo "samo nov $gzjson"
	cp $JSON2/$gzjson $TMP/json_new/
    elif [[ $(ls $OLD_DATA/probisdb/json/$gzjson 2>/dev/null) != "" ]]
    then
	echo "samo star $gzjson"
	cp $OLD_DATA/probisdb/json/$gzjson  $TMP/json_new/
    fi

} &>> $TMP/dbg105.txt
