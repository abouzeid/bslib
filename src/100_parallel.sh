#!/bin/bash


## determine which bslibfile to use
function determine_bslib {

    pdb_id=$1
    chain_id=$2
    clus_num=$3

    bslibfile_all=$TMP/bslib.txt
    bslibfile_new=$TMP/new_bslib.txt
    partial_bslibfile_all=$TMP/bslib${clus_num}.txt
    partial_bslibfile_new=$TMP/new_bslib${clus_num}.txt

    old_json_exists=0
    
    [[ $(grep ${pdb_id}${chain_id} $TMP/old_jsons.txt 2>/dev/null) != "" ]] && old_json_exists=1

    # old json does not exist or this is NOT an update
    [[ $old_json_exists == "0" ]] && [ -f $partial_bslibfile_all ] && echo $partial_bslibfile_all && return
    [[ $old_json_exists == "0" ]] && [ ! -f $partial_bslibfile_all ] && echo $bslibfile_all && return

    # this is an UPDATE and old json exists
    [[ $old_json_exists == "1" ]] && [ -f $partial_bslibfile_new ] && echo $partial_bslibfile_new && return
    [[ $old_json_exists == "1" ]] && [ ! -f $partial_bslibfile_new ] && echo $bslibfile_new && return
       
}

## if not exists, create a --partial-- bslib file from the last json
function create_partial_bslib_file {

    gzjson2=$1
    clus_num=$2
    bsfile=$3

    bslibfile_all=$TMP/bslib.txt
    bslibfile_new=$TMP/new_bslib.txt
    partial_bslibfile_all=$TMP/bslib${clus_num}.txt
    partial_bslibfile_new=$TMP/new_bslib${clus_num}.txt

    bslibfile=""
    
    if [[ $bsfile == $bslibfile_all ]]
    then
	bslibfile=$partial_bslibfile_all
    elif [[ $bsfile == $bslibfile_new ]]
    then
	bslibfile=$partial_bslibfile_new
    fi

    if [[ $bslibfile != "" ]] && [ ! -f $bslibfile ]
    then
	zegrep -o "pdb_id\":\"[^\"]*\"" $JSON2/$gzjson2|cut -f3 -d"\""|sed -r "s,^(.{4})(.{1})$,$REPDIR/\1\2.srf \2," > $bslibfile
    fi

}
    
## run probis
function probisit {
    line=$1
    clus_num=$(echo $line|cut -f1 -d"_")
    pdb=$(echo $line|cut -f2 -d"_"|sed -r "s/(.{4}):(.{1})/\L\1:\E\2/")
    pdb_id=${pdb:0:4}
    chain_id=${pdb:5:1}
    nosql2=$pdb_id$chain_id.nosql
    json2=$pdb_id$chain_id.json
    gzjson2=$pdb_id$chain_id.json.gz
    srffile=$pdb_id$chain_id.srf
    pdbname=$pdb_id$chain_id.pdb

    if [ ! -f $JSON2/$gzjson2 ] # if restarting, some jsons may already exist...
    then
	
	bslibfile=$(determine_bslib $pdb_id $chain_id $clus_num)

	echo "bslibfile $bslibfile pdb_id $pdb_id chain_id $chain_id"

	# compare each protein chain against surfaces of all binding sites
	gunzip -c $PDBDIR/all/pdb/pdb$pdb_id.ent.gz > $TMP/$pdbname

	probis -extract -f1 $TMP/$pdbname -c1 $chain_id -srffile $TMP/$srffile
	probis -ncpu $(( NUMPROBIS / 8 )) -surfdb -local -sfile $bslibfile -longnames -out $NOSQL2 -nosql $nosql2 -f1 $TMP/$srffile -c1 $chain_id

	probis -nofp -longnames -results -param $BASE/paramsf.inp -f1 $TMP/$pdbname -c1 $chain_id -nosql $NOSQL2/$nosql2 -out $JSON2 -json $json2

	gzip -f $NOSQL2/$nosql2 $JSON2/$json2

	create_partial_bslib_file $gzjson2 $clus_num $bslibfile

	rm -f $JSON2/*.cons.pdb $TMP/$pdbname $TMP/$srffile

    else
	echo "Already done... no action will be performed for pdb $pdb_id chain $chain_id"
    fi

} 


## main function
{

    line=$1
    clus_num=$(echo $line|cut -f1 -d"_")
    
    echo "Working on cluster number $clus_num..."
    echo "tmp $TMP clusfile $CLUSFILE json2 $JSON2 pdbdir $PDBDIR nosql2 $NOSQL2 srfdir $SRFDIR"

    # each cluster member compared against subset of bslibdb (determined by the json of representative) 
    egrep "^$clus_num\s+" $CLUSFILE|cut -f1,3|tr "\t" "_" > $TMP/members${clus_num}.txt

    for ln in $(cat $TMP/members${clus_num}.txt)
    do
	probisit $ln
    done

    
} &>> $TMP/dbg100.txt

