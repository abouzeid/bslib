#!/bin/bash

export CLUSFILE=$1

mkdir -p $NOSQL2 $JSON2

ls $REPDIR|sed -r "s,(.{4})(.{1})\.srf$,$REPDIR/\1\2.srf \2," > $TMP/bslib.txt

# prepare bslib file with new proteins only
if [[ $OLD_DATA != "" ]]
then
    
    ls $REPDIR|sed -r "s,(.*)\.srf,\1," > $TMP/pdbs_from_bslib.txt
    ls $OLD_DATA/$(basename $REPDIR)|sed -r "s,(.*)\.srf,\1," > $TMP/old_pdbs_from_bslib.txt

    grep -v -F -x -f $TMP/old_pdbs_from_bslib.txt $TMP/pdbs_from_bslib.txt|sed -r "s,(.{4})(.{1}),$REPDIR/\1\2.srf \2," > $TMP/new_bslib.txt

    ls $OLD_DATA/probisdb/json > $TMP/old_jsons.txt
    
fi

rep_file=rep$(basename $CLUSFILE)
sort -nusk1,1 $CLUSFILE|cut -f1,3|tr "\t" "_" > $TMP/$rep_file

# distribute files over worknodes
all_worknodes "ssh SUBST \"rm -f $TMP/dbg100.txt\"" "Removing debug file" $WORKNODE_LIST

all_worknodes "rsync -aRO --delete --rsync-path=\"mkdir -p $BASE $NOSQL2 $JSON2 && rsync\" --exclude=$(basename $BIODIR) --exclude=\"*.nosql*\" --exclude=\"*.json*\" --exclude=$(basename $LIGDIR) --exclude=$(basename $SRFDIR) --exclude=\"data-*\" $BASE SUBST:/" "Distribute files to nodes" $WORKNODE_LIST

all_worknodes "rsync -aRO --delete $JSON2 SUBST:/" "Distribute json.gz files (needed if restarting) to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $TMP/old_jsons.txt SUBST:/" "Distribute old_jsons file (if it exists) to nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO --delete $TMP/new_bslib.txt SUBST:/" "Distribute new_bslib file (if it exists) to nodes" $WORKNODE_LIST

# run parallel job
cat $TMP/$rep_file|parallel --env BASE --env PATH --env PDBDIR --env TMP --env NOSQL2 --env JSON2 --env REPDIR --env CLUSFILE --env NUMPROBIS --progress --delay 1 --load 100% --sshloginfile $WORKNODE_LIST '100_parallel.sh {}'


# collect the data from all hosts
all_worknodes "rsync -aRO SUBST:$NOSQL2 /" "Collecting nosql files from nodes" $WORKNODE_LIST
all_worknodes "rsync -aRO SUBST:$JSON2 /" "Collecting json files from nodes" $WORKNODE_LIST

