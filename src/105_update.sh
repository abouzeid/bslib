#!/bin/bash

CLUSFILE=$1

rm -f $TMP/dbg105.txt

# join old (if exists) and new jsons into one united new json that contains only proteins in new bslib.txt
if [[ $OLD_DATA != "" ]]
then

    mkdir -p $TMP/json_new

    cat $CLUSFILE|cut -f3|sed -r "s/(.{4}):(.{1})$/\L\1\E\2/"|parallel --progress --env PATH --env OLD_DATA --env BASE --env JSON2 --env TMP "105_parallel.sh"

    mv $JSON2 ${JSON2}_only_new
    mv $TMP/json_new $JSON2
fi

