
BEGIN{ 
    i=1
    conect_idx=0
} 

{

    if ($1 ~ "CONECT") {
	conect[conect_idx++] = $0
#	print $0
    }

    if ($1 ~ "ATOM" || $1 ~ "HETATM") { 

	regex="moltype="
	where = match($0, regex)

	if (where != 0) {

	    entity=substr($0, where + length(regex))
	    gsub(/ /, "", entity)
	    
#    print entity, " wtf ? ", i
#    print length(resn[entity])
	    
	    if (entity !~ /protein/ && entity !~ /nucleic/ && i != 1) { 
#	print entity, " next"
		next
	    }
	    
	    if (!(entity in resn))  {
		
#	    print entity
		
		resn[entity]=substr($0,18,3); 
		gsub(/ /, "", resn[entity])
#	    print "resn=",("[" resn[entity] "]")
		
		resi[entity]=substr($0,23,4)
		gsub(/ /, "", resi[entity])
		
#	    print "resn=",resn[entity]
#	    print "resi=",resi[entity]
	    }
	    
	    split(entity, ltype, "#")
	    
	    name=sprintf("%s_%s_%s_%d_%s_%d",ltype[1], pdb_id, resn[entity],resi[entity], substr($0,22,1),i)
	    
	    if (!(entity in sz))  {
		sz[entity] = 0
	    }
	    
#	print "name = ", name, " sz of entity = ", sz[entity]
	    contents[name][sz[entity]++] = $0;
	}
    }

    if ($1 ~ "ENDMDL") { 
#	print $1,"increasing"
	i++
    } 

}

END{ 
    for (c in contents) {
	for(d=0; d in contents[c]; d++) {
	    print contents[c][d] > (tmp_dir "/" c)
	}
	if (c !~ /protein/ && c !~ /nucleic/ && c !~ /water/ && c !~ /ion/) {
	    for (e in conect) {
		print conect[e] >  (tmp_dir "/" c)
	    }
	}
    }
}
