import json
import sys
import gzip
import filecmp
import os

# check if old and new JSON files are identical. If yes, it may still be that the bio files (with ligands) of the representatives
# clusters changed, in which case an update is still needed...

## argument 1 ... old json.gz file
## argument 2 ... new json.gz file
## argument 3 ... old lig/bio directory
## argument 4 ... new lig/bio directory

# check if the JSONs are identical (and if both exist)
if not os.path.exists(sys.argv[1]) or not os.path.exists(sys.argv[2]) or not filecmp.cmp(sys.argv[1], sys.argv[2]):
    sys.exit(1)

#read the old and new json files

with gzip.open(sys.argv[2], "rb") as f:
    data = json.loads(f.read().decode("ascii"))

# compare the representatives (their lig/bio files)
for d in data:
    file_name = d["pdb_id"] + ".pdb.gz"
    path_old = os.path.join(sys.argv[3] , file_name)
    path_new = os.path.join(sys.argv[4] , file_name)

    if not os.path.exists(path_old) or not os.path.exists(path_new) or not filecmp.cmp(path_old, path_new):
        sys.exit(2)

sys.exit(0)
