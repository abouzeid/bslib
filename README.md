# bslib
** A set of scripts to generate the "livedb" and "predb" binding site libraries. **

Start by typing run.sh in shell (cannot be done on laptop in reasonable time,
i.e., will take years on laptop if you are lucky).

After run.sh has finished, you are set to install the databases.

To install first set the INSTALLDIR in install[_pre].sh, and then run install[_pre].sh.
This installs the "livedb" or alternatively the "predb".

Depends on the following programs (included): 
* probis 
* probislite
* cluster-optics
* neighb
* gbio